// require("livescript");
var path = require('path');
webpack = require('webpack');
ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  // context: path.join(__dirname, 'src'),
  progress: true,
  entry: [
    './example/main.ls',
    './example/main.less'
  ],
  output: {
    path: path.join(__dirname, 'assets'),
    filename: 'bundle.js',
    pathinfo: true
  },
  devtool: 'eval',
  module: {
    loaders: [
      {
        test: /\.ls$/,
        loader: "livescript-loader"
      },
      { 
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&minetype=application/font-woff" },
      { 
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
        loader: "file-loader" 
      }, {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract("css!less")
      }, 
    ]
  },
  resolve: {
    modulesDirectories: ['node_modules', 'scripts'],
    extensions: [""].concat(['.js', '.ls'])
  },
  plugins: [
    new ExtractTextPlugin('bundle.css'),
    new webpack.ProvidePlugin({
      '$': 'jquery'
      // 'TextEditor': 'Texteditor.ls'
    }),
    // new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.DedupePlugin()
    // , new webpack.optimize.UglifyJsPlugin()
  ]
}
