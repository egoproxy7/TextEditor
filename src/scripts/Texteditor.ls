setElements = (el, value) ->
  el.innerHTML = '
  <div class="form_btns_wrap">
    <div class="form_btns">
      <div class="form_btngr">
        <div data-text="bold" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_полужирный(Ctrl+B)" class="form_icon fa fa-bold" data-leave="tooltip_rm"></div>
        <div data-text="italic" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_курсив(Ctrl+I)" class="form_icon fa fa-italic" data-leave="tooltip_rm"></div>
        <div data-text="underline" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_подчеркнутый(Ctrl+U)" class="form_icon fa fa-underline" data-leave="tooltip_rm"></div>
      </div>
      <div class="form_btngr">
        <div data-text="strikethrough" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_зачеркнутый" class="form_icon fa fa-strikethrough"></div>
        <div data-text="subscript" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_подстрочный" class="form_icon fa fa-subscript"></div>
        <div data-text="superscript" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_надстрочный" class="form_icon fa fa-superscript" data-leave="tooltip_rm"></div>
      </div>
      <div class="form_btngr">
        <div data-text="justifyleft" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_выравниевание слева" class="form_icon fa fa-align-left"></div>
        <div data-text="justifycenter" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_выравнивание по центру" class="form_icon fa fa-align-center"></div>
        <div data-text="justifyright" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_выравнивание справа" class="form_icon fa fa-align-right"></div>
      </div>
      <div class="form_btngr">
        <div data-text="insertorderedlist" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_нумерованный список" class="form_icon fa fa-list-ol"></div>
        <div data-text="insertunorderedlist" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_список" class="form_icon fa fa-list-ul"></div>
      </div>
      <div class="form_btngr">
        <div data-enter="tooltip_pure" data-tp="bottom_цвет фона" class="form_icon_color">
          <input data-text="backgroundcolor" type="color" data-input="text_bcolor" class="form_input_color">
        </div>
      </div>
      <div class="form_btngr">
        <div data-enter="tooltip_pure" data-tp="bottom_цвет текста" class="form_icon_color">
          <input data-text="textcolor" type="color" data-input="text_tcolor" class="form_input_color">
        </div> 
      </div>
      <div class="form_btngr">
        <select data-text="fontsize" data-input="text_size" data-enter="tooltip_pure" data-tp="bottom_размер шрифта" class="form_icon_select">
          <option value="1">10</option>
          <option value="2">13</option>
          <option value="3">16</option>
          <option value="4">18</option>
          <option value="5">24</option>
          <option value="6">32</option>
        </select> 
      </div>
      <div class="form_btngr">
        <select data-text="fontname" data-input="text_name" data-enter="tooltip_pure" data-tp="bottom_шрифты" class="form_icon_select">
          <option value="Arial">Arial</option>
          <option value="Calibri">Calibri</option>
          <option value="Comic Sans MS">Comic Sans MS</option>
        </select> 
      </div>
      <div class="form_btngr">
        <div data-text="h1" data-down="text_header" data-enter="tooltip_pure" data-tp="bottom_заголовок 1" class="form_icon fa fa-header" data-leave="tooltip_rm">1</div>
        <div data-text="h2" data-down="text_header" data-enter="tooltip_pure" data-tp="bottom_заголовок 2" class="form_icon fa fa-header" data-leave="tooltip_rm">2</div>
        <div data-text="h3" data-down="text_header" data-enter="tooltip_pure" data-tp="bottom_заголовок 3" class="form_icon fa fa-header" data-leave="tooltip_rm">3</div>
        <div data-text="h4" data-down="text_header" data-enter="tooltip_pure" data-tp="bottom_заголовок 4" class="form_icon fa fa-header" data-leave="tooltip_rm">4</div>
        <div data-text="h5" data-down="text_header" data-enter="tooltip_pure" data-tp="bottom_заголовок 5" class="form_icon fa fa-header" data-leave="tooltip_rm">5</div>
        <div data-text="h6" data-down="text_header" data-enter="tooltip_pure" data-tp="bottom_заголовок 6" class="form_icon fa fa-header" data-leave="tooltip_rm">6</div>
      </div>
      <div class="form_btngr">
        <div data-text="link" data-down="text_insertlink" data-enter="tooltip_pure" data-tp="bottom_вставить ссылку" class="form_icon fa fa-link"></div>
        <div data-text="anchor" data-down="text_insertanchor" data-enter="tooltip_pure" data-tp="bottom_вставить якорь" class="form_icon fa fa-anchor"></div>
        <div data-text="file" data-down="text_addimgs" data-enter="tooltip_pure" data-tp="bottom_загрузить изображение" class="form_icon fa fa-picture-o"></div>
        <div data-text="imgresizecontrol" data-down="text_imgresizecontrol" data-enter="tooltip_pure" data-tp="bottom_при нажатии левой кнопки мыши менять размер изображений" class="form_icon fa fa-square-o" data-leave="tooltip_rm"></div>
        <div data-down="text_addtm" data-enter="tooltip_pure" data-tp="bottom_добавить таблицу" class="form_icon form_icon_wl fa fa-table"></div>
        <div data-text="tableresizecontrol" data-down="text_tableresizecontrol" data-enter="tooltip_pure" data-tp="bottom_при нажатии левой кнопки мыши менять размер таблиц" class="form_icon fa fa-square-o" data-leave="tooltip_rm"></div>
      </div>
      <div class="form_btngr">
        <div data-text="undo" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_отменить" class="form_icon fa fa-undo"></div>
        <div data-text="redo" data-down="text_render" data-enter="tooltip_pure" data-tp="bottom_повторить" class="form_icon fa fa-repeat"></div>
      </div>
      <div data-down="text_setwin" data-enter="tooltip_pure" data-tp="bottom_полноэкранный режим" class="form_icon_right fa fa-expand"></div>
      <div class="clearfix"></div>
    </div>
    <div class="form_btns" style="display:none;clear:both;">
      <div class="form_btngr">
        <div data-text="horisontal_before_addcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_добавить ячейки сверху" class="form_icon fa fa-caret-square-o-up" data-leave="tooltip_rm">+</div>
        <div data-text="vertical_after_addcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_добавить ячейки справа" class="form_icon fa fa-caret-square-o-right" data-leave="tooltip_rm">+</div>
        <div data-text="horisontal_after_addcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_добавить ячейки снизу" class="form_icon fa fa-caret-square-o-down" data-leave="tooltip_rm">+</div>
        <div data-text="vertical_before_addcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_добавить ячейки слева" class="form_icon fa fa-caret-square-o-left" data-leave="tooltip_rm">+</div>
      </div>
      <div class="form_btngr">
        <div data-text="horisontal_before_rmcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_удалить ячейки сверху" class="form_icon fa fa-caret-square-o-up" data-leave="tooltip_rm">-</div>
        <div data-text="vertical_after_rmcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_удалить ячейки справа" class="form_icon fa fa-caret-square-o-right" data-leave="tooltip_rm">-</div>
        <div data-text="horisontal_after_rmcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_удалить ячейки снизу" class="form_icon fa fa-caret-square-o-down" data-leave="tooltip_rm">-</div>
        <div data-text="vertical_before_rmcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_удалить ячейки слева" class="form_icon fa fa-caret-square-o-left" data-leave="tooltip_rm">-</div>
      </div>
      <div class="form_btngr">
        <div data-text="horisontal_before_mcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_передвинуть ряд вверх" class="form_icon fa fa-caret-square-o-up" data-leave="tooltip_rm">~</div>
        <div data-text="vertical_after_mcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_передвинуть ряд вправо" class="form_icon fa fa-caret-square-o-right" data-leave="tooltip_rm">~</div>
        <div data-text="horisontal_after_mcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_передвинуть ряд вниз" class="form_icon fa fa-caret-square-o-down" data-leave="tooltip_rm">~</div>
        <div data-text="vertical_before_mcell" data-down="text_celledit" data-enter="tooltip_pure" data-tp="bottom_передвинуть ряд влево" class="form_icon fa fa-caret-square-o-left" data-leave="tooltip_rm">~</div>
      </div>
      <div class="form_btngr">
        <div data-enter="tooltip_pure" data-tp="bottom_цвет фона ячейки" class="form_icon_color">
          <input data-text="cellcolor" type="color" data-input="text_cellcolor" class="form_input_color">
        </div>
      </div>
      <div class="form_btngr">
        <div data-enter="tooltip_pure" data-tp="bottom_цвет контура ячейки" class="form_icon_color">
          <input data-text="bcellcolor" type="color" data-input="text_bcellcolor" class="form_input_color">
        </div>
      </div>
      <div class="form_btngr">
        <div data-enter="tooltip_pure" data-tp="bottom_цвет фона таблицы" class="form_icon_color">
          <input data-text="tablecolor" type="color" data-input="text_tablecolor" class="form_input_color">
        </div>
      </div>
      <div class="form_btngr">
        <div data-enter="tooltip_pure" data-tp="bottom_цвет контура таблицы" class="form_icon_color">
          <input data-text="btablecolor" type="color" data-input="text_btablecolor" class="form_input_color">
        </div>
      </div>
      <div class="form_btngr">
        <select data-text="bcollapse" data-input="text_bcollapse" data-enter="tooltip_pure" data-tp="bottom_граница траблицы и ячеек" class="form_icon_select">
          <option value="collapse">совместная</option>
          <option value="separate">раздельная</option>
        </select> 
      </div>
      <div class="form_btngr">
        <select data-text="brules" data-input="text_brules" data-enter="tooltip_pure" data-tp="bottom_границы" class="form_icon_select">
          <option value="all">все</option>
          <option value="groups">группа</option>
          <option value="cols">колонки</option>
          <option value="rows">строки</option>
          <option value="none">нет</option>
        </select> 
      </div>
      <div class="form_btngr">
        <div data-text="rmtable" data-down="text_rmtable" data-enter="tooltip_pure" data-tp="bottom_удалить всю таблицу" class="form_icon fa fa-trash" data-leave="tooltip_rm"></div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="form_btns" style="display:none;clear:both;">
      <div class="form_btngr">
        <div data-text="editlink" data-down="text_editlink" data-enter="tooltip_pure" data-tp="bottom_редактировать ссылку" class="form_icon fa fa-link" data-leave="tooltip_rm"></div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="form_btns" style="display:none;clear:both;">
      <div class="form_btngr">
        <select data-text="ialign" data-input="text_ialign" data-enter="tooltip_pure" data-tp="bottom_обтекание текстом" class="form_icon_select">
          <option value="left">справа</option>
          <option value="right">слева</option>
          <option value="none">нет</option>
        </select> 
      </div>
      <div class="form_btngr">
        <div data-text="rmimg" data-down="text_rmimg" data-enter="tooltip_pure" data-tp="bottom_удалить ихзображение" class="form_icon fa fa-trash" data-leave="tooltip_rm"></div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="form_text_wrap">
    <div data-blur="text_blur" data-paste="text_paste" contenteditable="true" class="form_text form_doc"></div>
  </div>
  <div class="form_textfooter">
    <div data-down="text_resizer" class="form_sizefooter fa fa-angle-double-down"></div>
  </div>
  <div class="form_text_screen">
    <div class="form_text_closer" data-down="text_screenoff">
      <button>отмена</button>
    </div>
    <div class="form_text_center">
      <div class="form_text_window"></div
    </div>
  </div>'
  btns = el.querySelectorAll('.form_btns')
  anchor =
    btnw: el.querySelector '.form_btns_wrap'
    btns: el.querySelectorAll '.form_icon'
    text: el.querySelector '.form_text'
    wrap: el.querySelector '.form_text_wrap'
    screen: el.querySelector '.form_text_screen'
    win: el.querySelector '.form_text_window'
    tmenu: btns[1]
    lmenu: btns[2]
    imenu: btns[3]
  for ind in el.querySelectorAll '.form_icon, .form_input_color, .form_icon_select'
    if ind.dataset.text
      anchor[ind.dataset.text] = ind

  anchor.text.setAttribute 'name', value
  return anchor


setEvents = (el, store) ->

  data = (obj, data) ->
    unless data 
      data = obj.el.getAttribute 'data-'+obj.ev.data
    if data.indexOf('_') > 0
      data = data.split("_")
      obj.store[data[0]][data[1]] obj

  ev = (e) ->
    data { el: @, ev: e, store: store }
    return false
  $doc = $(el)
  # $doc.off()
  $doc.on "click", "[data-click]", "click", ev
  $doc.on "mouseenter", "[data-enter]", "enter", ev
  $doc.on "mouseleave", "[data-leave]", "leave", ev
  $doc.on "mousedown", "[data-down]", "down", ev
  $doc.on "mouseup", "[data-up]", "up", ev
  $doc.on "keydown", "[data-kdown]", "kdown", ev
  $doc.on "keyup", "[data-kup]", "kup", ev
  $doc.on "submit", "[data-sub]", "sub", ev
  $doc.on "paste", "[data-paste]", "paste", ev
  $doc.on "focus", "[data-focus]", "focus", ev
  $doc.on "blur", "[data-blur]", "blur", ev
  $doc.on "wheel", "[data-wheel]", "wheel", ev
  # @$doc.on "scroll", "[data-scr]", "scr", @ev
  $doc.on "contextmenu", "[data-menu]", "menu", ev
  $doc.on "change", "[data-change]", "change", ev
  $doc.on "input", "[data-input]", "input", ev
  $doc.on "drop", "[data-drop]", "drop", ev
  $doc.on "dragover", "[data-dragover]", "dragover", ev
  
  $doc.on "forsize", "[data-forsize]", "forsize", ev
  # $doc.on "rollin", "[data-rollin]", "rollin", @ev

setTooltip =
  ###*
  # Простой
  # @method pure
  # @private
  ###
  pure: (obj) ->
    @clear()
    @render obj

  ###*
  # Слив
  # @method sink
  # @private
  ###
  sink: (obj) ->
    @clear()
    if $(obj.el).width() < $('.txt', obj.el).width()
      @render obj
    else
      tp = document.getElementById 'tooltip'
      tp.style.display = 'none'

  ###*
  # Горизонтальный Слив
  # @method sink
  # @private
  ###
  hsink: (obj) ->
    @clear()
    if $(obj.el).height() < $('.txt', obj.el).innerHeight()
      @render obj
    else
      tp = document.getElementById 'tooltip'
      tp.style.display = 'none'

  ###*
  # Вперед
  # @method render
  # @private
  ###
  render: (obj) ->
    $el = $(obj.el)
    offset = $el.offset()
    $el.attr 'data-leave','tooltip_rm'
    set = $el.attr('data-tp')
    if -1 != set.indexOf '_'
      split = $el.attr('data-tp').split('_')
      type = split[0]
      text = split[1]
    else
      type = set
      text = $el.text()
    if !tp = document.getElementById 'tooltip'
      $('body').prepend '<div id="tooltip" class="tooltip" style="display: none;"></div>'
      tp = document.getElementById 'tooltip'
    $tp = $(tp)
    $tp.html text
    switch type
    case 'left'
      top = offset.top
      left = offset.left - $tp.innerWidth()
    case 'top'
      top = offset.top - $tp.innerHeight()
      left = offset.left
    case 'bottom'
      top = offset.top + $el.innerHeight() + 1
      left = offset.left
    case 'right'
      top = offset.top
      left = offset.left + $el.innerWidth()
    case 'cursor'
      top = obj.ev.pageY
      left = obj.ev.pageX
    $tp.css(
      top : top
      left : left
      display : 'block')

  ###*
  # Чистка
  # @method clear
  # @private
  ###
  clear: ->
    if @timer
      clearTimeout(@timer)
      delete @timer

  ###*
  # Запустить таймер уборки
  # @method rm
  # @private
  ###
  rm: (obj) ->
    self = @
    go = ->
      self.remove()
      delete self.timer
    @timer = setTimeout(go, 250)

  ###*
  # Убрать
  # @method remove
  # @private
  ###
  remove: ->
    el = document.getElementById 'tooltip'
    if el
      el.style.display = 'none'

setMover =
  resize: (event, element, callback, dir, topWrapPx) ->
    bound = (max,num,min) ->
      if max <= num
        num = max
      else if num <= min
        num = min
      return num
    if event.button == 0
      # par = $(event.target).parents('[data-resize]')[0]
      # $child = $(par).children('[data-resize]')
      # if $child.length
      #   par = $child[0]
      css = element.style
      size = element.getBoundingClientRect()
      # dir = element.dataset['resize']
      arr = []
      isDirX = !dir or dir == "x"
      isDirY = !dir or dir == "y"
      if isDirX
        width = size.width - event.pageX
        minWidth = css.minWidth.replace 'px',''
        left = $(document).width() - size.left
      if isDirY
        height = size.height - event.pageY
        minHeigh = css.minHeight.replace 'px',''
        top = $(document).height() - size.top
        if topWrapPx
          top = top - topWrapPx
      # $forsize = $(par)
      resizer = (e) ->
        if e.which == 1
          if isDirX
            css.width = bound(left, e.pageX+width, minWidth) + "px"
          if isDirY
            css.height = bound(top, e.pageY+height, minHeigh) + "px"
          # if $forsize.length
          #   $forsize.trigger 'forsize'
        # else
        #   window.onmousemove = null
        #   if $.isFunction callback
        #     callback do
        #       width: css.width
        #       height: css.height
        #       top: css.top
        #       left: css.left
      # window.onmousemove = resizer
      $(window).on 'mousemove', resizer
      $(window).one 'mouseup', ->
        $(window).off 'mousemove', resizer
        if $.isFunction callback
          callback do
            width: css.width
            height: css.height
            top: css.top
            left: css.left

getHexRGBColor = (color) ->
  componentToHex = (c) ->
    c = +c
    hex = c.toString 16
    if hex.length is 1 then '0' + hex else hex

  rgbToHex = (r, g, b) -> 
    return '#' + componentToHex(r) + componentToHex(g) + componentToHex (b)

  color = color.match(/\(([^()]*)\)/)[1]
  color = color.split ','

  return rgbToHex $.trim(color[0]), $.trim(color[1]), $.trim(color[2])

dataURItoBlob = (dataURI) ->
  # convert base64/URLEncoded data component to raw binary data held in a string
  byteString = undefined
  if dataURI.split(',')[0].indexOf('base64') >= 0
    byteString = atob(dataURI.split(',')[1])
  else
    byteString = unescape(dataURI.split(',')[1])
  # separate out the mime component
  mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
  # write the bytes of the string to a typed array
  ia = new Uint8Array(byteString.length)
  i = 0
  while i < byteString.length
    ia[i] = byteString.charCodeAt(i)
    i++
  return new Blob([ ia ], type: mimeString)

module.exports = class TextEditor
  (settings) ->
    for ind of settings
      if !@[ind]
        @[ind] = settings[ind]
      else
        console.log 'такие установки мне не нужны'
    if @root
      if settings.root.tagName
        if $.isFunction settings.elements
          settings.elements!
        else
          @anchor = setElements settings.root, @elname
          setEvents settings.root, { text: @, tooltip: setTooltip, mover: setMover }
    else
      console.log 'текстовый редактор хочет знать, где рут?'
    if @val
      @setHTML @val
    @_enter!
    @_stayKey @anchor.text

  getHTML: ->
    return @anchor.text.innerHTML

  setHTML: (val) ->
    @anchor.text.innerHTML = val

  note: (obj) ->
    $el = $ obj.el
    $el.toggleClass 'fa-file-o fa-file-text-o'
    $el.parents('.form_field').children('.form_text').toggleClass 'form_note form_doc'

  render: (obj) ->
    selection = document.getSelection!
    if selection.isCollapsed 
      obj.el.classList.toggle 'form_icon_active'
    @_go obj.el.dataset.text, false, false

  _go: (command, param, val) ->
    @_goFocus()
    document.execCommand command, param, val

  _change: (e) ->
    console.log 'change'
    if e
      @_stayKey e.target
    if @onchange and $.isFunction @onchange
      @onchange @anchor.text

  _imageDown: (e) ->
    if @imgResize
      setMover.resize e, e.target, ~>
        @_change!
      return false
    else
      return true

  _tableDown: (e, el) ->
    if @tableResize
      setMover.resize e, el, ~>
        @_change!
      return false
    else
      return true

  _enter: ->
    @anchor.text.ondrop = (e) ~>
      @drag e
    @anchor.text.oninput = (e) ~>
      @_change e
    @anchor.text.onmousedown = (e) ~>
      switch e.target.tagName
      case 'IMG'
        return @_imageDown e
      case 'TABLE'
        return @_tableDown e, e.target
      case 'TD'
        return @_tableDown e, e.target.parentNode.parentNode.parentNode
      case 'TR'
        return @_tableDown e, e.target.parentNode.parentNode
      case 'TBODY'
        return @_tableDown e, e.target.parentNode
      else
        return true
    @anchor.text.onclick = (event) ~>
      @_stayKey event.target
      return true
    @anchor.text.onkeydown = (event) ~>
      switch event.keyCode
      case 9
        @_go 'insertHtml', false, '&emsp;'
        return false
      return true
    @anchor.text.onkeyup = (event) ~>
      switch event.keyCode
      case 13
        @_link event.target
      case 32
        range = document.getSelection!.getRangeAt 0
        text = range.commonAncestorContainer
        @_textLinkCheck event.target, text, true
      case 37,38,39,40
        @_stayKey event.target
      return true

  insertlink: (obj) ->
    @_insert obj, @_linkform('http://', window.getSelection().toString!), false, (wrap) ->
      if wrap.children[3].value
        href = wrap.children[3].value
      else
        href = 'http://'
      if wrap.children[1].value
        value = wrap.children[1].value
      else
        value = href
      node = document.createElement('a')
      node.innerHTML = value
      node.setAttribute 'href', href
      return node

  insertanchor: (obj) ->
    @_insert obj, @_anchorform(''), false, (form) ->
      node = document.createElement('img')
      if form.children[0].value
        node.classList.add 'text_editor_anchor_node', 'noselect'
        node.id = form.children[0].value
      return node

  editlink: (obj) ->
    anchor = @anchor.a
    @_insert obj, @_linkform(anchor.getAttribute('href'), anchor.innerText), false, (wrap) ->
      node = anchor
      node.innerHTML = wrap.children[1].value
      node.href = wrap.children[3].value
      return node

  _linkform: (href, text) ->
    html = '
      <div style="text-align: center;" >
        <label class="noselect"> Текст: </label>
        <input style="background-color:white" value="' + text + '"/>
        <label class="noselect"> Адрес: </label>
        <input style="background-color:white" value="' + href + '" data-enter="tooltip_pure" data-tp="bottom_для обычной ссылки http://адрес для якоря #имя" />
        <button type="button" autofocus >Сохранить</button>
      </div>'
    return html

  _anchorform: (id) ->
    html = '
      <div style="text-align: center;" >
        <label class="noselect"> Название: </label>
        <input style="background-color:white" value="' + id + '" data-enter="tooltip_pure" data-tp="bottom_имя должно быть уникальным" />
        <button type="button" autofocus >Сохранить</button>
      </div>'
    return html

  linkedit: (obj) ->
    linktarget = obj.el.parentNode.parentNode
    if linktarget
      linktarget.innerHTML = obj.el.0.value
      linktarget.href = obj.el.1.value
      # linktarget.removeAttribute 'id'

  blur: (obj) ->
    val = obj.el.innerHTML 
    if val.length
      @_link obj.el

  _link: (node) ->
    nodes = node.childNodes
    i = 0
    while i < nodes.length
      type = ''
      if nodes[i].nodeType == 1
        if nodes[i].nodeName != 'A' and nodes[i].innerHTML != ''
          @_link nodes[i]
      else if nodes[i].nodeType == 3
        @_textLinkCheck node, nodes[i]
      # console.dir nodes[i]
      i++

  _textLinkCheck: (parentNode, currentNode, space) ->
    oldValue = currentNode.nodeValue
    newValue = @_linkReplacer oldValue
    reg = /([</a>])+/ig
    stack = reg.test newValue
    if stack
      stack = false
      el = document.createElement 'div'
      el.innerHTML = oldValue
      parentNode.innerHTML = parentNode.innerHTML.replace(el.innerHTML, newValue)
      if space
        @setEndOfContenteditable parentNode

  _linkReplacer: (text) ->
    text = text.replace(/(^|[^\/\"\'\>\w])(http|https|ftp\:\/\/)(\S+)([\wа-яёЁ\/\-]+)/ig, '$1<a target="_blank" href="$2$3$4">$2$3$4</a>')
    text = text.replace(/(^|[^\/\"\'\>\w])(www\.)(\S+)([\wа-яёЁ\/\-]+)/ig, '$1<a target="_blank" href=\"http://$2$3$4\">$2$3$4</a>')
    return text

  setEndOfContenteditable: (obj) ->
    if document.createRange
      range = document.createRange!
      range.selectNodeContents obj
      range.collapse false
      selection = window.getSelection!
      selection.removeAllRanges!
      selection.addRange range
    else
      if document.selection
        range = document.body.createTextRange!
        range.moveToElementText obj
        range.collapse false
        range.select!
    return

  # _rangeSelectionChilds: (selection) ->
  #   if selection.type == 'Range'
  #     range = selection.getRangeAt(0)
  #     container = range.commonAncestorContainer
  #     if container.childNodes.length
  #       allWithinRangeParent = container.getElementsByTagName('*')
  #       allSelected = []
  #       i = 0
  #       while elm = allWithinRangeParent[i]
  #         # true для выборки частично выделенного элемента
  #         if selection.containsNode(elm, true)
  #           allSelected.push elm
  #         i++
  #       return allSelected
  #   return false

  _stayKey: (el) ->
    if el.nodeName != 'IMG'
      if selection = window.getSelection()
        if selection.focusNode
          node = selection.focusNode
          if node.nodeType == 3
            el = node.parentNode
        else
          el = @anchor.text
    #   @anchor.selection = @_rangeSelectionChilds selection

    @_makeWork @anchor.btns, false
    $el = $(el)
    $active = $([])

    style = getComputedStyle(el)

    switch style.fontSize
    case 'x-small', '10px'
      @anchor.fontsize.value = 1
    case 'small', '13px'
      @anchor.fontsize.value = 2
    case 'medium', '16px'
      @anchor.fontsize.value = 3
    case 'large', '18px'
      @anchor.fontsize.value = 4
    case 'x-large', '24px'
      @anchor.fontsize.value = 5
    case 'xx-large', '32px'
      @anchor.fontsize.value = 6
    default
      @anchor.fontsize.value = 3

    switch style.fontFamily                                                                                                     
    case 'Arial'
      @anchor.fontname.value = 'Arial'
    case 'Calibri'
      @anchor.fontname.value = 'Calibri'
    case 'Comic Sans MS'
      @anchor.fontname.value = 'Comic Sans MS'

    @anchor.backgroundcolor.value = getHexRGBColor style.backgroundColor
    @anchor.textcolor.value = getHexRGBColor style.color

    if 'bold' == style.fontWeight
      $active = $active.add $(@anchor.bold)
    if 'italic' == style.fontStyle
      $active = $active.add $(@anchor.italic)

    switch style.textDecoration
    case 'underline'
      $active = $active.add $(@anchor.underline)
    case 'line-through'
      $active = $active.add $(@anchor.strikethrough)

    switch style.verticalAlign
    case 'sub'
      $active = $active.add $(@anchor.subscript)
    case 'super'
      $active = $active.add $(@anchor.superscript)

    switch style.textAlign
    case 'left'
      $active = $active.add $(@anchor.justifyleft)
    case 'center'
      $active = $active.add $(@anchor.justifycenter)
    case 'right'
      $active = $active.add $(@anchor.justifyright)

    if $el.parents('ol').length
      $active = $active.add $(@anchor.insertorderedlist)
    if $el.parents('ul').length
      $active = $active.add $(@anchor.insertunorderedlist)

    switch el.tagName
    case 'H1'
      $active = $active.add $(@anchor.h1)
    case 'H2'
      $active = $active.add $(@anchor.h2)
    case 'H3'
      $active = $active.add $(@anchor.h3)
    case 'H4'
      $active = $active.add $(@anchor.h4)
    case 'H5'
      $active = $active.add $(@anchor.h5)
    case 'H6'
      $active = $active.add $(@anchor.h6)
    case 'TD'
      tmenu = 'block'
      @anchor.cell = el
    case 'A'
      lmenu = 'block'
      @anchor.a = el
    case 'IMG'
      imenu = 'block'
      @anchor.img = el
    if !imenu
      imenu = 'none'
    else
      if style.float
        @anchor.ialign.value = style.float
    if !tmenu
      $cell = $el.parents('td')
      if 0 < $cell.length
        tmenu = 'block'
        @anchor.cell = $cell[0]
      else
        tmenu = 'none'
        @anchor.cell = false
    if !lmenu
      $a = $el.parents('a')
      if 0 < $a.length
        lmenu = 'block'
        @anchor.a = $a[0]
      else
        lmenu = 'none'
        @anchor.a = false
    if @anchor.cell
      style = getComputedStyle(@anchor.cell)
      @anchor.cellcolor.value = getHexRGBColor style.backgroundColor
      @anchor.bcellcolor.value = getHexRGBColor style.borderColor
      if @anchor.table = @anchor.cell.parentNode.parentNode.parentNode
        style = getComputedStyle(@anchor.table)
        @anchor.tablecolor.value = getHexRGBColor style.backgroundColor
        @anchor.btablecolor.value = getHexRGBColor style.borderColor
        @anchor.bcollapse.value = style.borderCollapse
        @anchor.brules.value = @anchor.table.getAttribute 'rules'
    @anchor.tmenu.style.display = tmenu
    @anchor.lmenu.style.display = lmenu
    @anchor.imenu.style.display = imenu
    #TODO проверку на изменение стейта
    if @bigSize
      @_setWin!
    @_makeWork $active, true

  ialign: (obj) ->
    if @anchor.img
      @anchor.img.style.float = obj.el.value
      @_change!

  _makeWork: (el, bool) ->
    $(el).toggleClass 'form_icon_active', bool

  bcolor: (obj) ->
    @_go 'BackColor', false, obj.el.value
    # @_goFocus()
    # if !document.execCommand "HiliteColor", false, obj.el.value
    #   document.execCommand "BackColor", false, obj.el.value

  tcolor: (obj) ->
    @_go 'forecolor', false, obj.el.value
    # @_goFocus()
    # document.execCommand "forecolor", false, obj.el.value

  size: (obj) ->
    @_go 'fontSize', false, obj.el.value
    # @_goFocus()
    # document.execCommand 'fontSize', false, obj.el.value

  name: (obj) ->
    @_go 'fontName', false, obj.el.value
    # @_goFocus()
    # document.execCommand 'fontName', false, obj.el.value

  _goFocus: ->
    if window.getSelection.type = 'none'
      @anchor.text.focus()

  header: (obj) ->
    @_goFocus()
    if obj.el.classList.contains 'form_icon_active'
      val = 'div'
    else
      val = obj.el.dataset.text
    document.execCommand 'formatBlock', false, '<' + val + '>'

  _pasteForm: (htmlText) ->
    html = '
      <div style="text-align: center;" >
        <div>' + htmlText + '</div>
        <button type="button" autofocus >Сохранить</button>
        <div></div>
      </div>'
    return html

  paste: (obj) ->
    text = @anchor.text

    if obj.ev.originalEvent.clipboardData.getData
      val = obj.ev.originalEvent.clipboardData.getData(obj.ev.originalEvent.clipboardData.types[0])
      if -1 != val.indexOf 'img'
        @_insert obj, @_pasteForm(val), 'img', (form) ~>
          node = form.children[0]
          # node.onmousedown = (e) ~>
          #   @_imageDown e
          return node
      else if -1 < event.clipboardData.types.indexOf 'Files'
        files = obj.ev.originalEvent.clipboardData.items
        i = 0
        while i < files.length
          if files[i].kind == 'file' and files[i].type == 'image/png'
            imageFile = files[i].getAsFile()
            fileReader = new FileReader
            fileReader.onloadend = (e) ~>
              val = '<img src="' + e.target.result + '" width="80%" align="top" hspace="10px" vspace="10px" />'
              @_insert obj, @_pasteForm(val), 'img', (form) ~>
                node = form.children[0]
                # node.onmousedown = (e) ~>
                #   @_imageDown e
                return node
              return
            fileReader.readAsDataURL imageFile
            break
          i++
      else if -1 < event.clipboardData.types.indexOf 'text/html'
        val = obj.ev.originalEvent.clipboardData.getData('text/html')
        html = document.createElement 'div'
        html.innerHTML = val
        $(html).find('*').removeAttr('style').removeAttr('src').removeAttr('id').removeAttr('class')
        @_go 'insertHtml', false, html.innerHTML
      else
        @_go 'insertText', false, val

  _insert: (obj, html, upload, getNode) ->
    { text, screen } = @anchor

    @_goFocus()

    if window.getSelection && (sel = window.getSelection()).rangeCount
      range = sel.getRangeAt 0
      # range.collapse true

    @anchor.win.innerHTML = html
    button = @anchor.win.getElementsByTagName 'button'
    screen.style.display = 'block'

    fullend = (els) ->
      frag = document.createDocumentFragment!
      frag.appendChild els 
      range.insertNode frag
      screen.style.display = 'none'
      $(text).trigger 'input'
    halfend = (el) ->
      el.parentNode.firstChild.innerHTML = 'Загрузка...'
      el.style.display = 'none'

    if upload and @[upload + 'Url']
      button[0].onclick = (e) ~>
        halfend e.target
        @_sender getNode(button[0].parentNode), upload, fullend
        return false
    else
      button[0].onclick = (e) ->
        fullend getNode(button[0].parentNode)
        return false


  screenoff: (obj) ->
    @anchor.screen.removeAttribute 'style'

  addimgs: (obj) ->
    html = '<input type="file" accept="image/*" data-change="text_imginput" multiple="true" />'
    @_insert obj, @_pasteForm(html), 'img', (form) ~>
      node = form.children[2]
      # for ind in node.children
        # ind.onmousedown = (e) ~>
        #   @_imageDown e
      return node

  _filesToImg: (files) ->
    i = 0
    while f = files[i]
      if !f.type.match('image.*')
        i++
        continue
      reader = new FileReader
      reader.onload = (e) ~>
        win = @anchor.win
        img = document.createElement('img')
        img.setAttribute 'align', 'top'
        img.setAttribute 'hspace', '10px'
        img.setAttribute 'vspace', '10px'
        img.src = e.target.result
        #вставить в модальное окно
        @anchor.win.children[0].children[2].appendChild img
        return
      reader.readAsDataURL f
      i++

  imginput: (obj) ->
    @_filesToImg obj.ev.target.files

  drag: (event) ->
    if -1 != event.dataTransfer.getData('text/html').indexOf '<img'
      if !@imgUrl or -1 != event.dataTransfer.getData('text/html').indexOf 'data-upload="true"'
        event.dataTransfer.effectAllowed = 'move'
        return true
    if -1 < event.dataTransfer.types.indexOf 'Files'
      @_insert event, @_pasteForm('<div></div>'), 'img', (form) ~>
        node = form.children[2]
        return node
      if event.dataTransfer.files.length
        @_filesToImg event.dataTransfer.files
        return false
    return false

  addtm: (obj) ->
    menu = document.createElement("DIV")
    menu.classList.add 'table_choice_wrap'
    menu.dataset.down = 'text_rm'
    html = '<table class="table_choice">' + @_getChoiceTable(0, 0) + '</table>'
    menu.innerHTML = html
    obj.el.appendChild menu
    
  tmchoice: (obj) ->
    $td = $(obj.el)
    $tr = $td.parent()
    if obj.ev.data == 'enter'
      $tr.parent().html @_getChoiceTable($tr.index(), $td.index())
    else
      $tr.parents('.table_choice_wrap').remove()
      @_addTable($tr.index(), $td.index())

  _getChoiceTable: (trm, tdm) ->
    html = ''
    tr = 0
    while tr < 10
      td = 0
      html += '<tr>'
      while td < 10
        ev = 'enter'
        if tr > trm or td > tdm
          md = 'a'
        else
          md = 'b'
          if td == tdm and tr == trm
            ev = 'down'
        html += '<td class="table_choice_inner' + md + '" data-' + ev + '="text_tmchoice" ></td>'
        td += 1
      html += '</tr>'
      tr += 1
    html += '<tr><td></td><td colspan="8" >' + (trm + 1) + 'x' + (tdm + 1) + '</td><td class="fa fa-times"></td></tr>'
    return html

  _addTable: (trm, tdm) ->
    table = document.createElement("TABLE")
    table.setAttribute 'border', 0
    table.setAttribute 'rules', 'all'
    table.setAttribute 'bordercolor', '#000'
    table.style.borderCollapse = 'collapse'
    table.style.width = '300px'
    html = ''
    tr = 0
    while tr <= trm
      td = 0
      html += '<tr>'
      while td <= tdm
        html += '<td><br></td>'
        td += 1
      html += '</tr>'
      tr += 1
    table.innerHTML = html
    # html = '<div><br><table border="" bordercolor="#000" style="border-collapse: collapse; width: 100%;"><tbody>' + html + '</tbody></table><br></div>'
    # table.onmousedown = (e) ~>
    #   @_tableDown e
    if window.getSelection && (sel = window.getSelection()).rangeCount
      range = sel.getRangeAt 0
      range.collapse true 
      if !$(sel.focusNode).is @anchor.text
        if !$(sel.focusNode).parents().is @anchor.text
          range = false
    frag = document.createDocumentFragment!
    frag.appendChild table
    # див с пробелом в конце
    div = document.createElement("DIV")
    div.innerHTML = '&nbsp;'
    frag.appendChild div
    if range and range.insertNode
      range.insertNode frag
    else
      @anchor.text.appendChild frag
    @_change!
    # @_go 'insertHtml', false, html

  rm: (obj) ->
    $(obj.el).remove()
    @_change!

  addcell: (set, $tableCurrent) ->
    if set[1] == 'before'
      method = 'beforeBegin'
    else
      method = 'afterEnd'
    if set[0] == 'horisontal'
      html = ''
      tr = @anchor.cell.parentNode
      for ind in tr.children
        html += '<td>&nbsp;</td>'
      html = '<tr>' + html + '</tr>'
      tr.insertAdjacentHTML method, html
    else
      index = $tableCurrent.index()
      for ind in @anchor.cell.parentNode.parentNode.children
        ind.children[index].insertAdjacentHTML method, '<td>&nbsp;</td>'
    @_change!

  rmcell: (set, $tableCurrent) ->
    if set[1] == 'before'
      modifier = -1
    else
      modifier = 1
    if set[0] == 'horisontal'
      tr = @anchor.cell.parentNode
      index = $(tr).index()
      parent = tr.parentNode
      if el = parent.children[index + modifier]
        parent.removeChild(el)
    else
      index = $tableCurrent.index()
      for ind in @anchor.cell.parentNode.parentNode.children
        if el = ind.children[index + modifier]
          ind.removeChild(el)
    @_change!

  mcell: (set, $tableCurrent) ->
    if set[1] == 'before'
      modifier = -1
      method = 'beforeBegin'
    else
      modifier = 1
      method = 'afterEnd'
    if set[0] == 'horisontal'
      html = ''
      tr = @anchor.cell.parentNode
      index = $(tr).index()
      if el = tr.parentNode.children[index + modifier]
        el.insertAdjacentElement method, tr
    else
      index = $tableCurrent.index()
      for ind in @anchor.cell.parentNode.parentNode.children
        if el = ind.children[index + modifier]
          el.insertAdjacentElement method, ind.children[index]
    @_change!

  celledit: (obj) ->
    if @anchor.cell
      set = obj.el.dataset.text.split '_'
      @[set.pop()] set, $(@anchor.cell)

  _setCSS: (el, nodeName, prop, value) ->
    done = false
    if @anchor.selection
      for element in @anchor.selection
        if element.nodeName == nodeName
          done = true
          $(element).css prop, value
    if el and !done
      $(el).css prop, value
    @_change!

  cellcolor: (obj) ->
    @_setCSS @anchor.cell, 'TD', 'backgroundColor', obj.el.value

  bcellcolor: (obj) ->
    @_setCSS @anchor.cell, 'TD', 'borderColor', obj.el.value

  tablecolor: (obj) ->
    $(@anchor.table).find('td').css 'backgroundColor', obj.el.value
    @_setCSS @anchor.table, 'TABLE', 'backgroundColor', obj.el.value

  btablecolor: (obj) ->
    $(@anchor.table).find('td').css 'borderColor', obj.el.value
    @_setCSS @anchor.table, 'TABLE', 'borderColor', obj.el.value

  bcollapse: (obj) ->
    if @anchor.table
      @anchor.table.style.borderCollapse = obj.el.value
    @_change!

  rmtable: (obj) ->
    $(@anchor.cell).parents('TABLE').eq(0).detach()
    $(@anchor.text).trigger 'input'

  rmimg: ->
    $(@anchor.img).remove()
    $(@anchor.text).trigger 'input'

  brules: (obj) ->
    if @anchor.table
      @anchor.table.setAttribute 'rules', obj.el.value
    @_change!    

  tableresizecontrol: (obj) ->
    @tableResize = !@tableResize
    $(obj.el).toggleClass 'fa-check-square-o', @tableResize
    .toggleClass 'fa-square-o', !@tableResize
    $(@anchor.text).toggleClass 'form_movetableintext', !@tableResize
    .toggleClass 'form_movetableintext', @tableResize

  imgresizecontrol: (obj) ->
    @imgResize = !@imgResize
    $(obj.el).toggleClass 'fa-check-square-o', @imgResize
    .toggleClass 'fa-square-o', !@imgResize
    $(@anchor.text).toggleClass 'form_moveimgintext', !@imgResize
    .toggleClass 'form_moveimgintext', @imgResize

  resizer: (obj) ->
    setMover.resize obj.ev, @anchor.wrap, false, 'y'

  setwin: (obj) ->
    #следующая переменная для того чтобы в начальных настройках можно не указывать свой макс_класс
    maxSizeClass = 'form_text_wrap_max'
    if @maxSizeClass 
      maxSizeClass = maxSizeClass + '_' + @maxSizeClass 
    if @bigSize
      @anchor.wrap.style.top = 0
      obj.el.classList.remove 'form_icon_active'
      @root.classList.remove maxSizeClass
      @anchor.screen.classList.remove maxSizeClass
      @bigSize = false
    else
      @_setWin!
      obj.el.classList.add 'form_icon_active'
      @root.classList.add maxSizeClass
      @anchor.screen.classList.add maxSizeClass
      @bigSize = true

  _setWin: ->
    height = $(@anchor.btnw).height()
    @anchor.wrap.style.top = height + 'px'

  _sender: (html, type, callback) ->
    @sendImgStack = 0
    sendAll = (num, del) ->
      if del
        html.removeChild(del)
      if num == html.childElementCount
        callback html
    if html.childElementCount
      i = 0
      while i < html.childElementCount
        file = html.children[i]
        @['_' + type + 'Send'](file, sendAll)
        i++

  _imgSend: (file, sendAll) ->
    formData = new FormData!
    formData.append 'uploads', dataURItoBlob(file.getAttribute 'src'), file.getAttribute 'name'
    file.setAttribute 'src', ''
    if formData.getAll('uploads').length
      $.ajax {
        url: @imgUrl
        type: 'POST'
        data: formData
        processData: false
        contentType: false
        success: (res) ~>
          if res.path
            file.setAttribute 'src', res.path
            file.setAttribute 'data-upload', true
            sendAll ++@sendImgStack
        error: ~>
          sendAll @sendImgStack, file
        xhr: ->
          xhr = new XMLHttpRequest
          xhr.upload.addEventListener 'progress', ((evt) ->
            # if evt.lengthComputable
            #   percentComplete = evt.loaded / evt.total
            #   percentComplete = parseInt percentComplete * 100
            #   ($ '.progress-bar').text percentComplete + '%'
            #   ($ '.progress-bar').width percentComplete + '%'
            #   ($ '.progress-bar').html 'Done' if percentComplete is 100
            return ), false
          xhr
        }
